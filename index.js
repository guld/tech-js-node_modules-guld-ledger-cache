const { getFS } = require('guld-fs')
const path = require('path')
const home = require('user-home')
const spawn = require('guld-spawn').getSpawn()
const LEDGERDIR = path.join(home, 'ledger')
const LEDGERPATH = path.join(LEDGERDIR, 'journal.cache')
const EQUITYPATH = path.join(LEDGERDIR, 'equity.cache')
var fs

async function addToJournal (tx) {
  fs = fs || await getFS()
  await fs.appendFile(LEDGERPATH, `\n${tx}\n`)
}

async function setJournal (j) {
  fs = fs || await getFS()
  return fs.writeFile(LEDGERPATH, j)
}

async function _cacheJournal (p, re) {
  fs = fs || await getFS()
  var data = await fs.readFile(p, 'utf8')
  if (!re || re.test(data)) await addToJournal(data)
}

async function cacheJournal (p = LEDGERDIR, re) {
  fs = fs || await getFS()
  var list = await fs.readdir(p)
  await Promise.all(list.map(async (l) => {
    if (l.startsWith('.')) return
    var newpath = `${p}/${l}`
    if (l.endsWith('.dat') || l.endsWith('.db')) return _cacheJournal(newpath, re)
    var stats = await fs.stat(newpath)
    if (stats.isDirectory()) {
      return cacheJournal(newpath, re)
    }
  }))
  return setEquity()
}

async function setEquity () {
  fs = fs || await getFS()
  return spawn('ledger', '', ['-f', LEDGERPATH, 'equity', '-o', EQUITYPATH])
}

module.exports = {
  addToJournal: addToJournal,
  setJournal: setJournal,
  cacheJournal: cacheJournal,
  setEquity: setEquity
}
