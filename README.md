# guld-ledger-cache

[![source](https://img.shields.io/badge/source-bitbucket-blue.svg)](https://bitbucket.org/guld/tech-js-node_modules-guld-ledger-cache) [![issues](https://img.shields.io/badge/issues-bitbucket-yellow.svg)](https://bitbucket.org/guld/tech-js-node_modules-guld-ledger-cache/issues) [![documentation](https://img.shields.io/badge/docs-guld.tech-green.svg)](https://guld.tech/lib/guld-ledger-cache.html)

[![node package manager](https://img.shields.io/npm/v/guld-ledger-cache.svg)](https://www.npmjs.com/package/guld-ledger-cache) [![travis-ci](https://travis-ci.org/guldcoin/tech-js-node_modules-guld-ledger-cache.svg)](https://travis-ci.org/guldcoin/tech-js-node_modules-guld-ledger-cache?branch=guld) [![lgtm](https://img.shields.io/lgtm/grade/javascript/b/guld/tech-js-node_modules-guld-ledger-cache.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/guld/tech-js-node_modules-guld-ledger-cache/context:javascript) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-guld-ledger-cache/status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-guld-ledger-cache) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-guld-ledger-cache/dev-status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-guld-ledger-cache?type=dev)

Guld ledger cache management.

### Install

##### Node

```sh
npm i guld-ledger-cache
```


### License

MIT Copyright isysd
