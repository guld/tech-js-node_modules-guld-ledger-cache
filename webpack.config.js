module.exports = [
  {
    target: 'web',
    entry: {
      index: './index.js'
    },
    output: {
      filename: 'guld-ledger-cache.min.js',
      path: __dirname,
      library: 'LedgerCache',
      libraryTarget: 'var'
    }
  }
]
